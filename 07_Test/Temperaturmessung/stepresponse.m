% brief: get step response
% author: Gerald Ebmer
% date: 03.07.2019

%% header
close all;
clear all;
clc;
instrreset; %instrfind



%% init
iron = serial('COM3','baudrate',115200);
fopen(iron);



%% measurement loop
N = 1000; % nr of meas points
Tset=zeros(N,1);
Tmeas = zeros(N,1);
time = zeros(N,1);
for i=1:N
   % read iron voltage
   %"Timestamp_s,SetPointTemp_gradC,IronTemp_gradC,ControlOutput\r\n"
   line = fscanf(iron);
   data = strsplit(line,',');
   time(i) = str2double(data{1});
   Tset(i) = str2double(data{2});
   Tmeas(i) = str2double(data{3});

   % wait
   pause(0.5);
   % flush serial buffer
   fread(iron, iron.BytesAvailable);
   fgets(iron);
   clc;
   fprintf(1,'%d: set: %f\tmeas: %f\n',i,Tset(i),Tmeas(i));
end



%% close
fclose(iron);
delete(iron);

%% save and plot
save('stepresponse.mat','Tmeas','Tset');

plot(time,Tmeas,time,Tset);
grid on;
xlabel('time/s');
ylabel('Temperatur/GradC');