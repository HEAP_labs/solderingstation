% brief: thermo couple coefficients with LMS
% author: Gerald Ebmer
% date: 03.07.2019

%% header
close all;
clear all;
clc;
instrreset; %instrfind

addpath('D:\OneDrive\MATLAB_Werkstatt\SCPIDmm');

%% init
iron = serial('COM3','baudrate',115200);
fopen(iron);

dmm = init20xxDMM('ASRL8::INSTR',1); % visa object

%% measurement loop
N = 300; % nr of meas points
u_iron=zeros(N,1);
T = zeros(N,1);
for i=1:N
   % read iron voltage
   %"Timestamp_s,SetPointTemp_gradC,IronTemp_gradC,ControlOutput\r\n"
   line = fscanf(iron);
   data = strsplit(line,',');
   u_iron(i) = str2double(data{3});
   % read iron temp with dmm
   T(i) = getTemperature(dmm);
   % wait
   pause(0.5);
   % flush serial buffer
   fread(iron, iron.BytesAvailable);
   fgets(iron);
   clc;
   fprintf(1,'%d: u: %f\tT: %f\n',i,u_iron(i),T(i));
end

%% lms
lms

%% clsoe
fclose(iron);
delete(iron);
