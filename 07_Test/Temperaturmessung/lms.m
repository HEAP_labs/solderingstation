close all;
clear all;
clc;



%% g�ltigen datenbereich ausschneiden
load('data1.mat');
u1 = u_iron(50:180);
T1 = T(50:180);
x1 = 1:length(u1);
plot(x1,T1/max(T1),x1,u1/max(u1));
grid on;

figure
load('data2.mat');
u2 = u_iron(65:140);
T2 = T(65:140);
x2 = 1:length(u2);
plot(x2,T2/max(T2),x2,u2/max(u2));
grid on;

u3 = u_iron(165:202);
T3 = T(165:202);

u4 = u_iron(215:260);
T4 = T(215:260);

u = [u1;u2;u3;u4];
T = [T1;T2;T3;T4];

%% lms
[m,n] = size(u);
S = [ones(m,1),u,u.*u,u.*u.*u];

p = (S'*S)\(S'*T);

y = S*p;

x = 1:m;
plot(x,T,x,y);
grid on;
legend('gemessen','berechnet');
xlabel('Datenpunkte/1');
ylabel('Temperatur/GradC');