var modules =
[
    [ "Application", "df/d29/group___application.html", "df/d29/group___application" ],
    [ "System_Core", "d3/dfc/group___system___core.html", "d3/dfc/group___system___core" ],
    [ "STM32F0xx_API", "de/ddd/group___s_t_m32_f0xx___a_p_i.html", "de/ddd/group___s_t_m32_f0xx___a_p_i" ],
    [ "MCU_Library", "db/d40/group___m_c_u___library.html", "db/d40/group___m_c_u___library" ]
];