var _timer_8h =
[
    [ "CH1", "d8/df6/group___timer___exported___constants.html#ga90643238cf4f49aae5f0ab215ada7889", null ],
    [ "CH2", "d8/df6/group___timer___exported___constants.html#ga92dc73af14a6902eadd21bdee033cbfb", null ],
    [ "CH3", "d8/df6/group___timer___exported___constants.html#ga9a593b4f2e9cc1ab563a99ccc361ac2e", null ],
    [ "CH4", "d8/df6/group___timer___exported___constants.html#gabb9059a95eaaeef166368aeb2ef74887", null ],
    [ "API_PWM_set_dutycycle", "dc/d7b/group___timer.html#ga7165fe021c9e7b3e8d6bf18873df1880", null ],
    [ "API_PWM_start", "dc/d7b/group___timer.html#gacf893466ef0883963ced073b0807240b", null ],
    [ "API_TIM_start", "dc/d7b/group___timer.html#gabc8c05da4b67c2963e4fa5bbad20c952", null ],
    [ "API_TIM_stop", "dc/d7b/group___timer.html#ga78bf85bb15b023b7977a06544f5e1a3c", null ],
    [ "API_TIM1", "dc/d7b/group___timer.html#ga44cbbcb5baae0dcd7cd3d6b05ef6a3b0", null ],
    [ "API_TIM14", "dc/d7b/group___timer.html#ga62067e8b7db63cd139dd145d5dec0408", null ],
    [ "API_TIM15", "dc/d7b/group___timer.html#ga891b65df878953659149bce39d48d0cb", null ],
    [ "API_TIM16", "dc/d7b/group___timer.html#ga742832bc4695d3b0b819732482fee502", null ],
    [ "API_TIM17", "dc/d7b/group___timer.html#ga799df59595648377e69da3edd5eae016", null ],
    [ "API_TIM3", "dc/d7b/group___timer.html#gab93f0efa0956354fe65f6cd1846b89b8", null ]
];