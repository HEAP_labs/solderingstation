var _s_p_i_8h =
[
    [ "SPI_MODE_0", "d6/dfe/group___s_p_i___exported___constants.html#ga1ec07ad94d5f6276c1c0b41d0550fe52", null ],
    [ "SPI_MODE_1", "d6/dfe/group___s_p_i___exported___constants.html#gaf2af9f45539491c9753960535dc20fde", null ],
    [ "SPI_MODE_2", "d6/dfe/group___s_p_i___exported___constants.html#ga8fa3edd8616f8803490c93f09243a612", null ],
    [ "SPI_MODE_3", "d6/dfe/group___s_p_i___exported___constants.html#ga6a091da9f9011457fe28ab25f64c858d", null ],
    [ "API_SPI_DMA_init", "dd/d3c/group___s_p_i.html#ga2d4f089590595dedff78a7637a2eb833", null ],
    [ "API_SPI_DMA_send", "dd/d3c/group___s_p_i.html#ga342c6c4e2360f0c8bb5110ad84949fca", null ],
    [ "API_SPI_DMA_send_and_receive", "dd/d3c/group___s_p_i.html#ga2d3166a379ddc7223382a7649f3fd944", null ],
    [ "API_SPI_DMA_send_then_receive_statemachine", "dd/d3c/group___s_p_i.html#ga1c9b0499d6a75f5fd28b2dbd1254a38b", null ],
    [ "API_SPI_DMA_TIM_start_continious_tx", "dd/d3c/group___s_p_i.html#ga1b24f1c7339cae5d9efcababbe90d281", null ],
    [ "API_SPI_DMA_TIM_stop_continious_tx", "dd/d3c/group___s_p_i.html#ga50dcf46aecfb1f59aaa8e8a72f40adaa", null ],
    [ "API_SPI_init", "dd/d3c/group___s_p_i.html#ga442635c0f487e970ef63ac591aa70fc4", null ],
    [ "API_SPI_receive_byte", "dd/d3c/group___s_p_i.html#gaa5f5283efc84ecd331b455abf8c38e39", null ],
    [ "API_SPI_send_byte", "dd/d3c/group___s_p_i.html#gaee7c655673f111cba8a7e025b806f878", null ],
    [ "API_SPI_send_then_receive_statemachine", "dd/d3c/group___s_p_i.html#gabe73227fde84b91ca8719a73e409b084", null ],
    [ "API_SPI1", "dd/d3c/group___s_p_i.html#ga4fab1028ffeb845cdbf5703bbf606667", null ],
    [ "API_SPI2", "dd/d3c/group___s_p_i.html#ga175224d22caf84931572da85c58e4a63", null ]
];