var struct_l_i_b__hw__signal__s =
[
    [ "i_signal", "da/df3/struct_l_i_b__hw__signal__s.html#aad2254899bd2bfa80098d9fd407eb5c4", null ],
    [ "m_o_debounced", "da/df3/struct_l_i_b__hw__signal__s.html#af803d98de9cca5ca5e7347353a8d571f", null ],
    [ "o_debounced", "da/df3/struct_l_i_b__hw__signal__s.html#ab5181608cd29076e8bc6385d701385c8", null ],
    [ "o_edge", "da/df3/struct_l_i_b__hw__signal__s.html#aeb0ac0d3bdbca1ecb44ff8ae8159664b", null ],
    [ "o_neg_edge", "da/df3/struct_l_i_b__hw__signal__s.html#abce98d365cefa0887a2a9a2e76c58af5", null ],
    [ "o_pos_edge", "da/df3/struct_l_i_b__hw__signal__s.html#ad60442e9be30262888155b024083d829", null ],
    [ "portpin", "da/df3/struct_l_i_b__hw__signal__s.html#a97c1f70b7b05b4ade43760a4df8a6210", null ],
    [ "t_off_s", "da/df3/struct_l_i_b__hw__signal__s.html#a77d22dca0398afff53b7aa9a3a7edbd9", null ],
    [ "t_on_s", "da/df3/struct_l_i_b__hw__signal__s.html#aaf9e95dda48dd093231f4f823eb5c05d", null ]
];