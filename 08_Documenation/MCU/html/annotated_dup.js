var annotated_dup =
[
    [ "API_ADC_type_t", "d1/da0/struct_a_p_i___a_d_c__type__t.html", "d1/da0/struct_a_p_i___a_d_c__type__t" ],
    [ "API_GPIO_type_t", "d6/d90/struct_a_p_i___g_p_i_o__type__t.html", "d6/d90/struct_a_p_i___g_p_i_o__type__t" ],
    [ "API_SPI_type_t", "d1/d0e/struct_a_p_i___s_p_i__type__t.html", "d1/d0e/struct_a_p_i___s_p_i__type__t" ],
    [ "API_TIM_type_t", "da/de0/struct_a_p_i___t_i_m__type__t.html", "da/de0/struct_a_p_i___t_i_m__type__t" ],
    [ "LIB_7S_character_s", "d0/d12/struct_l_i_b__7_s__character__s.html", "d0/d12/struct_l_i_b__7_s__character__s" ],
    [ "LIB_7S_display_assembly_s", "d6/d6d/struct_l_i_b__7_s__display__assembly__s.html", "d6/d6d/struct_l_i_b__7_s__display__assembly__s" ],
    [ "LIB_7S_format_s", "d1/def/struct_l_i_b__7_s__format__s.html", "d1/def/struct_l_i_b__7_s__format__s" ],
    [ "LIB_7S_segment_bit_position_s", "d0/d06/struct_l_i_b__7_s__segment__bit__position__s.html", "d0/d06/struct_l_i_b__7_s__segment__bit__position__s" ],
    [ "LIB_7S_segment_portpin_s", "d0/d8e/struct_l_i_b__7_s__segment__portpin__s.html", "d0/d8e/struct_l_i_b__7_s__segment__portpin__s" ],
    [ "LIB_7S_SPI_communication_s", "d6/d60/struct_l_i_b__7_s___s_p_i__communication__s.html", "d6/d60/struct_l_i_b__7_s___s_p_i__communication__s" ],
    [ "LIB_hw_signal_s", "da/df3/struct_l_i_b__hw__signal__s.html", "da/df3/struct_l_i_b__hw__signal__s" ]
];