% Projekt: Lötstation
% Autor: Gerald Ebmer
% Datum: 07.07.2019
% (c) HEAP Engineering GmbH 2019

%% Header
close all;
clear all;
clc;

%% Parameter


%% Modell der Strecke
Ta = 0.1;
T0 = 20; % Umgebungstemperatur
Ts = 130; % Thermische Zeitkonstante
Tend = 250; % Endtemperatur bei Sprungantwortmessung
R = 2.8; % Widerstand Lötspitze
U = 6; % Spannungswert bei Sprungantwortmessung
P = U^2/R;
R_th = (Tend-T0)/P;
C_th = Ts/R_th;

s = tf('s');
% Übertragungsfunktion der Strecke: 
% Eingang [Heizleistung] = W
% Ausgang [Temperatur] = K
Gs = R_th/(1+s*R_th*C_th); 
% bode(Gs);
sysk = ss(Gs);
sysd = c2d(sysk,Ta);
% Anfangsbedingung
x0 = T0/sysk.C;
% Messrauschen
parSys.NP = 100; % Rauschleistung
parSys.A = sysk.A;
parSys.B = sysk.B;
parSys.C = sysk.C;
parSys.D = sysk.D;
parSys.T0 = T0;
parSys.x0 = x0;

%% Regler
[C_pi,info] = pidtune(sysk,'PID');
Cd_pi = c2d(C_pi,Ta);
parReg.P = Cd_pi.Kp;
parReg.I = Cd_pi.Ki;
parReg.Ta = Ta;

options = bodeoptions;
options.PhaseVisible='off';

[sysk_mag,sysk_phase,W,~,~] = bode(sysk);
[ol_mag,ol_phase] = bode(C_pi*sysk,W);

sysk_mag = 20*log10(sysk_mag);
ol_mag = 20*log10(ol_mag);

fhandle = figure;
semilogx(W,sysk_mag(:),'linewidth',2);
hold on;
semilogx(W,ol_mag(:),'linewidth',2);
grid on;
legend('Strecke','offener Regelkreis');
xlabel('omega/(rad/s)');
ylabel('Amplitude/dB');
title('Bode Diagramm');

% matlab2tikz('figurehandle',fhandle,'filename','BodePlot.tikz');

%% Kalmann Filter
% kontinuierlich
% xp = A*x + B*u + G*w;
% y = C*x + D*u + H*w + v;
parKal.Qn = 0.1*eye(1); % Kovarianzmatrix der Prozessstörung w
parKal.Rn = 0.1*eye(1); % Kovarianzmatrix des Messrauschens v
parKal.Ta = Ta;
G = ones(1); % Kopplung des Prozessrauschen auf den Prozess
H = zeros(1,1); % Auswirkung des Prozessrauschens auf Messgroessen
sys_kalman = ss(sysk.A,[sysk.B G],sysk.C,[sysk.D H]);
% diskreter Kalmanfilter
% xp = A*x + B*u + G*w;
% y = C*x + D*u + v; --> kein direkter Durchgriff von w auf Ausgang!
[~,parKal.KK,~,~,~] = kalmd(sys_kalman,parKal.Qn,parKal.Rn,parKal.Ta);
% [~,parKal.KK,~,~,~] = kalman(sys_kalman,parKal.Qn,parKal.Rn);
parKal.Asysd = sysd.A;
parKal.Bsysd = sysd.B;
parKal.Csysd = sysd.C;
parKal.x0 = x0;

%% Simulation

%% Plots
