/**
  * @file    application.c 
  * @author  Andreas Hirtenlehner
  * @brief   Implementation of the application code
  */

/* Includes ------------------------------------------------------------------*/
#include "application.h"
#include "arm_math.h"

/** @addtogroup Application
  * @{
  */

/** @defgroup Tasks 
  * @brief realtime tasks
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
q15_t adc_data[6];
double heater_duty_cycle = 0.0;
double amb_temp          = 22.0;
double setpoint_temp     = 0.0;
double iron_temp         = 0.0;
double u_iron_v      		 = 0.0;
double u_battery_v       = 0.0;
uint8_t cartridge_inserted = 0;

double battery_charge_lt[] = 
{
	3.0,
	3.1,
	3.2,
	3.3,
	3.4,
	3.5,
	3.6,
	3.7,
	3.8
};

/* Private function prototypes -----------------------------------------------*/
void taskclass1(void);
void taskclass1_shifted(void);
void taskclass2(void);
void soft_rt_taskclass(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Tasks_Private_Functions
  * @{
  */

double voltage2temp(double u_V){
	const double p[] = {21.270650837939310, 0.037352301685528, -2.916656193405051e-06, 1.940023251850759e-10};
	const double u_uV = u_V * 1e6;
	const double Toff = 25.0; // Temperatur waehrend Sensoridentifikation 
	double T = ((p[2]+p[3]*u_uV)*u_uV+p[1])*u_uV + p[0]; // Horner Schema fuer Rechengenauigkeit
	return T - Toff;
}

double controller(double e){
	static double me = 0, mu = 0, u = 0;
	const double Ta = T_PERIOD2_S;
	const double kp = 10.0;
	const double ki = 0.001;
	const double Unom = 14.4; // nominale Spannung
	const double R = 2.8; // Widerstand Heizelement
	const double u_max = Unom*Unom*R;
	const double u_min = 0.0;
	double e_aw;
	/*** PI - Regler mit anti windup ***/
	// anti windup
	const double u_aw = mu + kp*e + (Ta*ki-kp)*me;
	if(u_aw > u_max){
		e_aw = (u_max - mu - (Ta*ki-kp)*me)/kp;
	}else if(u_aw < u_min){
		e_aw = (u_min - mu - (Ta*ki-kp)*me)/kp;
	}else{
		e_aw = e;
	}
	// PI - Regler
	u = mu + kp*e_aw + (Ta*ki-kp)*me;
	mu = u;
	me = e_aw;
	return u;
}

double Power2DutyCycle(double u){
	// u .. Stellgroesse, [u] = W
	const double Unom = u_battery_v; // nominale Spannung
	const double R = 2.8; // Widerstand Heizelement
	static double Uout, dc;
	
	if(u > 0){
		Uout = sqrt(u*R);
		dc = Uout/Unom;
		if (dc > 1.0)
			dc = 1.0;
		else if (dc < 0.0)
			dc = 0.0;
	}
	else{
		dc = 0.0;
	}
	
	return dc;
}

/**
  * @brief  fastest taskclass with highest priority (called periodically)
  */
void taskclass1()
{
#ifdef __USING_7S
	LIB_7S_digit_increment();
#endif // __USING_7S

#ifdef __USING_WWDG
  WWDG_SetCounter(0x7F);
#endif // __USING_WWDG
}

/**
  * @brief  fastest taskclass with highest priority (called periodically after taskclass1)
  */
void taskclass1_shifted()
{	
	static uint32_t timer_ticks = 0;

#ifdef __USING_ADC
	API_ADC_DMA_start_single_conv(&API_ADC1);
#endif // __USING_ADC
	
	cartridge_inserted = !API_getDI(CARTRIDGE_DETECT_PP);
	
#ifdef __USING_TIMER
	TIM_SetAutoreload(TIM1, timer_ticks);
	TIM_Cmd(TIM1, ENABLE);
#endif

#ifdef __USING_ADC
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOSMP));
	ADC_ClearFlag(ADC1, ADC_FLAG_EOSMP);
#endif // __USING_ADC
	
	u_iron_v = adc_data[AMPLIFIER_ADC_CH] / 4095.0 * g_adc_ref_v / AMPLIFIER_GAIN;
	iron_temp = voltage2temp(u_iron_v) + amb_temp;			 // y: measured value
	const double temp_diff = setpoint_temp - iron_temp;  // e: control error
	
	/*** P - Regler ***/
	//heater_duty_cycle = HEATER_CONTROL_GAIN * temp_diff; // u: control output
	
	/*** PI - Regler ***/
	heater_duty_cycle = Power2DutyCycle(controller(temp_diff));

	// control output limitation
	if(heater_duty_cycle > 1.0 - (ADC_CONV_TIME_S + SETTLING_TIME_S) / T_PERIOD1_S) 
	{
			heater_duty_cycle = 1.0 - (ADC_CONV_TIME_S + SETTLING_TIME_S) / T_PERIOD1_S;
	}
	else if(heater_duty_cycle < 0)
	{
			heater_duty_cycle = 0;
	}
	// else;
	
	timer_ticks = API_TIM16.period_timer_ticks * (ADC_CONV_TIME_S / T_PERIOD1_S + heater_duty_cycle);

#ifdef __USING_WWDG
  WWDG_SetCounter(0x7F);
#endif // __USING_WWDG
}

/**
  * @brief  taskclass (called periodically)
  */
void taskclass2()
{
  static uint8_t iniOK = 0;
	static uint8_t shutdown;
	static uint8_t m_shutdown;
	static uint8_t startup;
	static uint8_t battery_cell_count;
	static uint8_t battery_low;
	uint8_t cartridge_discarded;
	static uint8_t m_cartridge_discarded;

	static double t_show_s;
	static double time_to_live_s;
	static double t_on_s;
	
	static parameters_t param;
	uint8_t no_cartridge_string[] = { characters.minus, characters.minus, characters.minus };
	uint8_t low_bat_string[] = { characters.L, characters.o, characters.b };
	static double m_setpoint_temp;

  static LIB_hw_signal_t hw_signal_button0 					= { BUTTON0_PP };
  static LIB_hw_signal_t hw_signal_button1 					= { BUTTON1_PP };
  static LIB_hw_signal_t hw_signal_button2					= { BUTTON2_PP };
  static LIB_hw_signal_t hw_signal_button3 					= { BUTTON3_PP };
	static LIB_hw_signal_t hw_signal_cartridge_holder = { CARTRIDGE_HOLDER_PP };
	
	static uint8_t smps_enable;
	static uint8_t m_smps_enable;
	
	u_battery_v  = adc_data[BATTERY_ADC_CH] * g_adc_ref_v / 4095.0 * (10.0 + 1.0) / 1.0;
	const double amb_temp_raw = NTC_T0 * NTC_B_CONST / (NTC_B_CONST + NTC_T0 * log(NTC_R_SERIES * adc_data[NTC_ADC_CH] / (4095.0 - adc_data[NTC_ADC_CH]) / NTC_R0)) + ABSOLUTE_ZERO_C;

	/* read buttons */
	LIB_hw_signal_read(&hw_signal_button0, 					T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_button1, 					T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_button2, 					T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_button3, 					T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_cartridge_holder, T_PERIOD2_S);
	
	/* read battery cell count */
	if(!iniOK) { battery_cell_count = u_battery_v / 3.3; }
	// else;
	
	/* check if battery is low */
	if(!iniOK)                                                  { battery_low = 0; }
	else if(u_battery_v / battery_cell_count <= LOW_BAT_CELL_V) { battery_low = 1; }
	// else;
	
	/* power up */
	if(!iniOK) 															{ startup = 1; }
	else if(!hw_signal_button0.o_debounced) { startup = 0; }
	// else;
	
	/* power down */
	if(!iniOK)																					             { shutdown = 0; }
	else if(!startup && hw_signal_button0.t_on_s >= LONG_PRESS_TIME) { shutdown = 1; }
	else if(hw_signal_button0.t_off_s > T_PERIOD2_S)		             { shutdown = 0; }
	// else;
	
	if(!iniOK)																					{ smps_enable = 1; }
	else if(shutdown && !hw_signal_button0.o_debounced) { smps_enable = 0; }
	else if(!shutdown && m_shutdown)  									{ smps_enable = 0; }
	else if(time_to_live_s <= 0)						            { smps_enable = 0; }
	// else;
	
#ifdef PRINT_TEMPERATURE
	API_USART_send_string(&API_USART1, "%.2f,%.0f,%.2f,%.2f\r\n", t_on_s, setpoint_temp, iron_temp, heater_duty_cycle);
#endif
	
#ifdef PRINT_TEMPERATURE_VOLTAGE
	API_USART_send_string(&API_USART1, "%.2f,%.0f,%.2f,%.2f\r\n", t_on_s, setpoint_temp, u_iron_v*1e6, heater_duty_cycle);
#endif
		
	if(!iniOK) { cartridge_discarded = 0;                                       }
	else       { cartridge_discarded = !hw_signal_cartridge_holder.o_debounced; }
	
	if(!iniOK) 					   			{ API_setDO(LED_PP, Bit_RESET);                        }
	else if(cartridge_inserted) { API_setDO(LED_PP, (BitAction) !cartridge_discarded); }
	else                   			{ API_setDO(LED_PP, Bit_RESET);                        }
	// else;
	
	/* filter ambient temperature */
	amb_temp = 0.1*amb_temp + 0.9*amb_temp_raw;

	/* parameter to show */
	if(!iniOK)                            						  						 { param = NO_CARTRIDGE;         }
	else if(battery_low)                                             { param = LOW_BAT;              }
	else if(startup && cartridge_inserted)													 { param = IRON_TEMPERATURE;     }
	else if(!startup && hw_signal_button0.o_pos_edge)  	 						 { param = SETPOINT_TEMPERATURE; } 
	else if(!startup && hw_signal_button0.t_on_s >= LONG_PRESS_TIME) { param = DEBUG_VALUE;          } 
	else if(hw_signal_button1.o_neg_edge) 													 { param = SETPOINT_TEMPERATURE; }
	else if(hw_signal_button2.o_neg_edge) 													 { param = SETPOINT_TEMPERATURE; }
	else if(hw_signal_button3.o_neg_edge) 													 { param = SETPOINT_TEMPERATURE; }
	else if(t_show_s > PARAM_SHOW_TIME_S && cartridge_inserted)			 { param = IRON_TEMPERATURE;     }
	else if(t_show_s > PARAM_SHOW_TIME_S) 			 								     { param = NO_CARTRIDGE;         }
	// else;
	
	/* temp config */
	if(!iniOK) 												 																	    			  				             	{ m_setpoint_temp  = TEMP_STARTUP;    }
	else if(!startup && hw_signal_button0.o_pos_edge && setpoint_temp - TEMP_BIG_STEP >= TEMP_LIMIT_MIN) 	{ m_setpoint_temp -= TEMP_BIG_STEP;   }
	else if(!startup && hw_signal_button0.o_pos_edge)																										 	{ m_setpoint_temp  = TEMP_LIMIT_MIN;  }
	else if(shutdown && !m_shutdown) 																																		 	{ m_setpoint_temp += TEMP_BIG_STEP;   } // correct value after entering in debug mode
  else if(hw_signal_button1.o_neg_edge && setpoint_temp - TEMP_SMALL_STEP >= TEMP_LIMIT_MIN) 						{ m_setpoint_temp -= TEMP_SMALL_STEP; }
	else if(hw_signal_button1.o_neg_edge)																										   						{ m_setpoint_temp  = TEMP_LIMIT_MIN;  }
	else if(hw_signal_button1.t_off_s >= LONG_PRESS_TIME) 																		 						{ m_setpoint_temp  = SETPOINT_TEMP_1; }
  else if(hw_signal_button2.o_neg_edge && setpoint_temp + TEMP_SMALL_STEP <= TEMP_LIMIT_MAX) 						{ m_setpoint_temp += TEMP_SMALL_STEP; }
	else if(hw_signal_button2.o_neg_edge)																										   						{ m_setpoint_temp  = TEMP_LIMIT_MAX;  }
	else if(hw_signal_button2.t_off_s >= LONG_PRESS_TIME) 																		 						{ m_setpoint_temp  = SETPOINT_TEMP_2; }
  else if(hw_signal_button3.o_neg_edge && setpoint_temp + TEMP_BIG_STEP <= TEMP_LIMIT_MAX)   						{ m_setpoint_temp += TEMP_BIG_STEP;   }
	else if(hw_signal_button3.o_neg_edge)																										   						{ m_setpoint_temp  = TEMP_LIMIT_MAX;  }
	else if(hw_signal_button3.t_off_s >= LONG_PRESS_TIME) 																		 						{ m_setpoint_temp  = SETPOINT_TEMP_3; }
	// else;
	
	/* temp config */
	if(!iniOK) 									                                 { setpoint_temp = TEMP_STARTUP;    }
	else if(battery_low)                                         { setpoint_temp = 0;               }
	else if(cartridge_discarded && m_setpoint_temp > TEMP_SLEEP) { setpoint_temp = TEMP_SLEEP;      }
	else												                                 { setpoint_temp = m_setpoint_temp; }
	
	/* on time */
	if(!iniOK) { t_on_s  = 0;           }
	else 			 { t_on_s += T_PERIOD2_S; }
	
	/* time to show a parameter */
	if(!iniOK) 		                         	{ t_show_s = 0; 										 }
	else if( hw_signal_button0.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_button1.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_button2.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_button3.o_debounced) { t_show_s = 0; 										 }
	else if(param == DEBUG_VALUE)						{ t_show_s = 0; 										 }
	else if(param != IRON_TEMPERATURE) 			{ t_show_s = t_show_s + T_PERIOD2_S; }
	else if(param != NO_CARTRIDGE)    			{ t_show_s = t_show_s + T_PERIOD2_S; }
	else 					                          { t_show_s = 0; 										 }
	
	/* time to switch off */
	if(!iniOK)                                            { time_to_live_s  = TIME_TO_LIFE_S; }
	else if( hw_signal_button0.m_o_debounced)             { time_to_live_s  = TIME_TO_LIFE_S; }
	else if(!hw_signal_button1.m_o_debounced)             { time_to_live_s  = TIME_TO_LIFE_S; }
	else if(!hw_signal_button2.m_o_debounced)             { time_to_live_s  = TIME_TO_LIFE_S; }
	else if(!hw_signal_button3.m_o_debounced)             { time_to_live_s  = TIME_TO_LIFE_S; }
	else if(cartridge_discarded != m_cartridge_discarded) { time_to_live_s  = TIME_TO_LIFE_S; }
	else                                                  { time_to_live_s -= T_PERIOD2_S;    }
	
	/* seven segment output */
	if(shutdown) 													 { LIB_7S_SPI_clear();																														      	 }
	else if(param == LOW_BAT)              { LIB_7S_set_format(3, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_string(low_bat_string);      }
	else if(param == IRON_TEMPERATURE)     { LIB_7S_set_format(1, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(iron_temp);           }
	else if(param == SETPOINT_TEMPERATURE) { LIB_7S_set_format(1, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(m_setpoint_temp);     }
  else if(param == AMB_TEMPERATURE)      { LIB_7S_set_format(1, 1, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(amb_temp);            }
	else if(param == BATTERY_VOLTAGE)      { LIB_7S_set_format(1, 2, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(u_battery_v);   			 }
	else if(param == DEBUG_VALUE)          { LIB_7S_set_format(1, 1, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(amb_temp);            }
	else if(param == NO_CARTRIDGE)         { LIB_7S_set_format(3, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_string(no_cartridge_string); }
	else 														       { LIB_7S_SPI_clear();                             																	       }
	
	/* SMPS enable */
	if(!iniOK) 															{ API_setDO(SMPS_ENABLE_PP, Bit_SET);   }
	else if( smps_enable && !m_smps_enable) { API_setDO(SMPS_ENABLE_PP, Bit_SET);   }
	else if(!smps_enable &&  m_smps_enable) { API_setDO(SMPS_ENABLE_PP, Bit_RESET); }
	// else;
	
	m_smps_enable = smps_enable;
	m_shutdown = shutdown; 
	m_cartridge_discarded = cartridge_discarded;
	
	iniOK = 1;
	 
#ifdef __USING_IWDG
  IWDG_ReloadCounter();
#endif // __USING_IWDG
}

/**
  * @brief  slowest taskclass (soft-realtime; called periodically in taskclass2)
  */
void soft_rt_taskclass()
{
	
}

/**
  * @brief startup - function, called in the main module
*/
void startup_sequence(void)
{
  SystemCoreClockUpdate();
	
	/* hold LDO enable */
	API_setDO(SMPS_ENABLE_PP, Bit_SET);
	
	/* enable battery measurement */
	API_setDO(BATTERY_MEASURE_PP, Bit_RESET);
	
	/* UART for printf */
#ifdef __USING_USART
	API_USART_init(&API_USART1, USART_TX_PP, USART_RX_PP, USART_BAUDRATE,1);
	
#ifdef PRINT_TEMPERATURE
	API_USART_send_string(&API_USART1, "Timestamp_s,SetPointTemp_gradC,IronTemp_gradC,ControlOutput\r\n");
#endif
	
#ifdef PRINT_TEMPERATURE_VOLTAGE
	API_USART_send_string(&API_USART1, "Timestamp_s,SetPointTemp_gradC,IronTemp_uV,ControlOutput\r\n");
#endif
	
#endif // __USING_USART

#ifdef __USING_ADC
  g_adc_ref_v = 3.3;
	API_ADC_DMA_init(	&API_ADC1, 
										(uint16_t*)adc_data, 
										ADC_SampleTime_239_5Cycles, 
										3, 
										BATTERY_ADC_PP,
										AMPLIFIER_ADC_PP, 
										NTC_ADC_PP );
#endif // __USING_ADC

#ifdef __USING_TIMER
	#ifdef __USING_7S
		#ifdef __USING_SPI
			LIB_7S_set_display_assembly(3, COMMON_ANODE);
			LIB_7S_set_segment_bit_position(3, 4, 6, 7, 8, 2, 1, 5);
			LIB_7S_set_digit_portpin(3, DIGIT1_PP, DIGIT2_PP, DIGIT3_PP);
			LIB_7S_set_format(1, 1, RIGHT, 0);
		
			LIB_7S_SPI_init(&API_SPI1, 
											SPI_MOSI_PP, 
											SPI_CLK_PP, 
											LE_PP, 
											N_OE_PP, 
											SPI_BaudRatePrescaler_128, 
											SPI_MODE_0);
			LIB_7S_SPI_DMA_TIM_start_continious_tx(&API_TIM16, T_PERIOD1_S);
		#endif // __USING_SPI
	#endif // __USING_7S

  API_TIM_start(&API_TIM16, TIM_IT_CC1, T_PERIOD1_S, 0);
  API_PWM_start(&API_TIM16, CH1, N_OE_PP, T_PERIOD1_S, 0.5, 0xFF, TIM_OCPolarity_High);
  API_TIM_start(&API_TIM14, TIM_IT_Update, T_PERIOD2_S, 5);
  API_PWM_start(&API_TIM1, CH1, HEATER_PP, T_PERIOD1_S, ADC_CONV_TIME_S/T_PERIOD1_S, 0, TIM_OCPolarity_Low);
	TIM_SelectOnePulseMode(TIM1, TIM_OPMode_Single);
	
#endif // __USING_TIMER

#ifdef __USING_IWDG
  API_IWDG_init(); //updated in taskclass2
#endif // __USING_IWDG

#ifdef __USING_WWDG  
  API_WWDG_init(); //updated in taskclass1
#endif // __USING_WWDG
}


#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
      /* e.g. write a character to the LCD */
   
      return ch;
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 
