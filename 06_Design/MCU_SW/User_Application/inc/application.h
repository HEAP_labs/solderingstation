/**
  * @file    application.h
  * @author  Andreas Hirtenlehner
  * @brief   Header file for the application module
  */

#ifndef __APPLICATION_H
#define __APPLICATION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <math.h>

/** @addtogroup Application
  * @{
  */

/** @addtogroup Tasks 
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  parameters to show on display
  */
	
typedef enum parameters_e
{
	BATTERY_VOLTAGE = 0,
	AMB_TEMPERATURE,
	IRON_TEMPERATURE,
	SETPOINT_TEMPERATURE,
	DEBUG_VALUE,
	CLEAR,
	NO_CARTRIDGE,
	LOW_BAT,
} parameters_t;

/* Exported constants --------------------------------------------------------*/
/** @defgroup Tasks_Exported_Constants
  * @{
  */ 

/**
  * @brief taskclass period time definitions [s]
  * @{
  */

#define T_PERIOD1_S         ((double) 5e-3)
#define T_PERIOD2_S         ((double) 5e-2)
#define T_PERIOD_SOFT_RT_S  ((double) 1.0)
	
/**
  * @}
  */ 
	
/**
  * @brief user definitions
  * @{
  */

//#define PRINT_TEMPERATURE
//#define PRINT_TEMPERATURE_VOLTAGE

#define TIME_TO_LIFE_S	  ((double) 300.0)	
#define PARAM_SHOW_TIME_S	((double) 2.0)
	
#define TEMP_LIMIT_MAX    ((double) 450.0)
#define TEMP_LIMIT_MIN    ((double) 0.0)
#define TEMP_STARTUP      ((double) 320.0)
#define TEMP_SLEEP        ((double) 120.0)
	
#define TEMP_SMALL_STEP   ((double) 10.0)
#define TEMP_BIG_STEP     ((double) 50.0)
	
#define SETPOINT_TEMP_1   ((double) 280.0) 
#define SETPOINT_TEMP_2   ((double) 340.0) 
#define SETPOINT_TEMP_3   ((double) 400.0) 
	
#define LONG_PRESS_TIME   ((double) 0.5)
	
#define LOW_BAT_CELL_V    ((double) 3.0)
	
#define ABSOLUTE_ZERO_C   ((double) -273.15)
#define NTC_B_CONST       ((double) 3900.0)
#define NTC_R0            ((double) 10e3)
#define NTC_T0            ((double) 25.0 - ABSOLUTE_ZERO_C)	
#define NTC_R_SERIES      ((double) 10e3)

#define ADC_CONV_TIME_S   ((double) 100e-6)
#define SETTLING_TIME_S   ((double) 2e-3)
	
#define HEATER_CONTROL_GAIN ((double) 0.1)

/**
  * @}
  */ 

/**
  * @brief portpin definitions
  * @{
  */

#define BUTTON0_PP 					((API_GPIO_type_t*) &PF6)
#define BUTTON1_PP 					((API_GPIO_type_t*) &PC15)
#define BUTTON2_PP 					((API_GPIO_type_t*) &PC14)
#define BUTTON3_PP 					((API_GPIO_type_t*) &PC13)

#define CARTRIDGE_DETECT_PP	((API_GPIO_type_t*) &PB8)
#define CARTRIDGE_HOLDER_PP	((API_GPIO_type_t*) &PB7)

#define SMPS_ENABLE_PP 			((API_GPIO_type_t*) &PF7)
#define BATTERY_MEASURE_PP 	((API_GPIO_type_t*) &PA0)

#define LED_PP						  ((API_GPIO_type_t*) &PB9)

#define SPI_MOSI_PP 				((API_GPIO_type_t*) &PB5)
#define SPI_MISO_PP 				((API_GPIO_type_t*) &PB4)
#define SPI_CLK_PP 					((API_GPIO_type_t*) &PB3)
#define LE_PP 							((API_GPIO_type_t*) &PA15)
#define N_OE_PP 						((API_GPIO_type_t*) &PB6)

#define DIGIT1_PP 					((API_GPIO_type_t*) &PA12)
#define DIGIT2_PP 					((API_GPIO_type_t*) &PB15)
#define DIGIT3_PP 					((API_GPIO_type_t*) &PA11)

#define BATTERY_ADC_PP      ((API_GPIO_type_t*) &PA1)
#define AMPLIFIER_ADC_PP    ((API_GPIO_type_t*) &PA2)
#define NTC_ADC_PP          ((API_GPIO_type_t*) &PA3)

#define HEATER_PP						((API_GPIO_type_t*) &PA8)
#define USART_TX_PP					((API_GPIO_type_t*) &PA9)
#define USART_RX_PP					((API_GPIO_type_t*) &PA10)
#define USART_BAUDRATE 			115200

/**
  * @brief ADC Channels
  * @{
  */

#define BATTERY_ADC_CH 			((uint8_t) 0)
#define AMPLIFIER_ADC_CH 		((uint8_t) 1)
#define NTC_ADC_CH 					((uint8_t) 2)

/**
  * @}
  */ 
	
/**
  * @brief Amplifier definitions
  * @{
  */
	
#define AAF_RES							((double) 10e3) /* R1A */
#define AAF_CAP							((double) 1e-6) /* C5A */
#define AAF_CUTOFF_FREQ     ((double) 1.0 / (2.0 * PI * AAF_RES * AAF_CAP))	

#define GAIN_RES						((double)  1e3) /* R5A,  Datasheet: R1 */
#define FEEDBACK_RES				((double)  2e5) /* R6A,  Datasheet: R2 */
#define AMPLIFIER_GAIN 			((double) (1.0 + (FEEDBACK_RES / GAIN_RES)))

/**
  * @}
  */ 

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __APPLICATION_H
